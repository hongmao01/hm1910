package cn.jasonone.resolver;

import cn.hutool.core.util.StrUtil;
import cn.jasonone.model.ModelAndView;
import cn.jasonone.model.ModelMap;
import lombok.Data;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * 视图解析器
 */
@Data
public class JspViewResolver implements ViewResolver {

    private static final String REDIRECT_PREFIX = "redirect:";

    private String prefix;

    private String suffix;

    @Override
    public void handler(ModelAndView modelAndView, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String viewName = modelAndView.getViewName();
        if (StrUtil.isBlank(viewName)) {
            throw new NullPointerException("视图名称不能为空");
        }
        if (viewName.toLowerCase().startsWith(REDIRECT_PREFIX)) {
            viewName = viewName.substring(REDIRECT_PREFIX.length());
            if(viewName.toLowerCase().startsWith("http:")
            ||viewName.toLowerCase().startsWith("https:")){
                response.sendRedirect(viewName);
            }else{
                response.sendRedirect(request.getContextPath()+viewName);
            }
        } else {
            for (Map.Entry<String, Object> entry : modelAndView.entrySet()) {
                request.setAttribute(entry.getKey(), entry.getValue());
            }
            request.getRequestDispatcher(prefix + modelAndView.getViewName() + suffix).forward(request, response);
        }
    }
}
