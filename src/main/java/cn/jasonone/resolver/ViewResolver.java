package cn.jasonone.resolver;

import cn.jasonone.model.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface ViewResolver {

    void setPrefix(String prefix);

    void setSuffix(String prefix);

    void handler(ModelAndView modelAndView,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;
}
