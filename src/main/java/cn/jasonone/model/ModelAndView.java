package cn.jasonone.model;

import lombok.Data;

@Data
public class ModelAndView extends ModelMap {
    private String viewName;

    public ModelAndView(){
        this(null);
    }

    public ModelAndView(String viewName){
        this.viewName=viewName;
    }
}
