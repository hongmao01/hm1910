package cn.jasonone.model;

import java.util.HashMap;

public class ModelMap extends HashMap<String,Object> implements Model{
    @Override
    public void addObject(String name,Object value){
        this.put(name,value);
    }
}
