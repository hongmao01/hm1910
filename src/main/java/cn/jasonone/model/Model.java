package cn.jasonone.model;

import java.util.Map;

public interface Model extends Map<String,Object> {
    public void addObject(String name,Object value);

}
