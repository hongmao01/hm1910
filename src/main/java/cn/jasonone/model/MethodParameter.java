package cn.jasonone.model;

import cn.hutool.core.convert.Convert;
import lombok.Data;

import java.lang.reflect.Array;
import java.lang.reflect.Field;

@Data
public class MethodParameter {
    private String name;
    private Object value;
    private Object obj;
    private Field field;

    private Class<?> type;

    public MethodParameter(){}

    public MethodParameter(String name){
        this.name=name;
    }

    public MethodParameter(Field field) {
        this.name = field.getName();
        this.field=field;
        this.type=field.getType();
    }

    public Object getValue(){
        if(value == null || (value.getClass().isArray()&&Array.getLength(value)==0)){
            return null;
        }
        // userInfo.createBy
        if(type.isInstance(value)){
            return value;
        }
        if(type.isArray()){
            Object array = Array.newInstance(type.getComponentType(), Array.getLength(value));
            for (int i = 0; i < Array.getLength(value); i++) {
                Array.set(array,i,Convert.convert(type.getComponentType(),Array.get(value,i)));
            }
            return array;
        }
        return Convert.convert(type,Array.get(value,0));
    }
}
