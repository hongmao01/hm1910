package cn.jasonone.handler;

import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import cn.jasonone.annotation.RequestParam;
import cn.jasonone.model.MethodParameter;
import cn.jasonone.model.Model;
import cn.jasonone.model.ModelAndView;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;

@Component
public class HandlerParameters {

    private final List<String> types = new ArrayList<>();

    {
        types.add(Byte.class.getName());
        types.add(Character.class.getName());
        types.add(Short.class.getName());
        types.add(Integer.class.getName());
        types.add(Long.class.getName());
        types.add(Float.class.getName());
        types.add(Double.class.getName());
        types.add(Boolean.class.getName());
        types.add(String.class.getName());
    }

    //

    public boolean hasBasciType(Class<?> type) {
        if (type.isPrimitive()) {
            return true;
        }
        if (type.isArray()) {
            return hasBasciType(type.getComponentType());
        }
        if(type.getPackage().getName().startsWith("java")
        ||type.getPackage().getName().startsWith("sun.")){
            return true;
        }
        return types.contains(type.getName());
    }

    public Object[] getParameters(ModelAndView modelAndView, Method method, HttpServletRequest request, HttpServletResponse response) throws IllegalAccessException {
        //获取请求中所有的参数列表
        Map<String, String[]> requestParameters = getRequestParameters(request);
        // 获取方法的入参列表
        Parameter[] parameters = method.getParameters();
        // 构建参数对象列表
        if (parameters != null) {
            Object[] args = new Object[parameters.length];
            for (int i = 0; i < parameters.length; i++) {
                Parameter parameter = parameters[i];
                Class<?> type = parameter.getType();
                if(ServletRequest.class.isAssignableFrom(type)){
                    args[i]=request;
                }else if(ServletResponse.class.isAssignableFrom(type)){
                    args[i]=response;
                }else if(HttpSession.class.isAssignableFrom(type)){
                    args[i]=request.getSession();
                }else if(Map.class.isAssignableFrom(type)){
                    args[i]=modelAndView;
                }
                else{
                    MethodParameter[] methodParameters = getParameterName("",parameter,type, requestParameters);
                    args[i] = getMethodParameterValue(methodParameters);
                }
            }
            return args;
        }
        // 参数名称  参数值  参数对象
        // userName  admin  null
        // userName  admin  userInfo
        return new Object[0];
    }

    private Object getMethodParameterValue(MethodParameter[] methodParameters) throws IllegalAccessException {
        if (methodParameters != null) {
            if (methodParameters.length > 0) {
                Object obj = null;
                for (MethodParameter methodParameter : methodParameters) {
                    obj = methodParameter.getObj();
                    if (obj == null) {
                        return methodParameter.getValue();
                    } else {
                        Field field = methodParameter.getField();
                        field.setAccessible(true);
                        field.set(obj, methodParameter.getValue());
                    }
                }
                return obj;
            }
        }
        return null;
    }

    public boolean hasPrefix(Map<String,String[]> requestParameters,String prefix){
        Set<String> keys = requestParameters.keySet();
        for (String key : keys) {
            if(key != null && key.startsWith(prefix)){
                return true;
            }
        }
        return false;
    }

    private MethodParameter[] getParameterName(String prefix,AnnotatedElement annotatedElement,Class<?> type, Map<String, String[]> requestParameters) {
        //检测注解
        if (annotatedElement.isAnnotationPresent(RequestParam.class) && hasBasciType(type)) {
            RequestParam requestParam = AnnotationUtils.getAnnotation(annotatedElement, RequestParam.class);
            Map<String, Object> attributes = AnnotationUtils.getAnnotationAttributes(requestParam);
            String name = (String) attributes.get("name");
            if (StrUtil.isNotBlank(name)) {
                MethodParameter methodParameter = new MethodParameter(prefix+name);
                methodParameter.setValue(requestParameters.get(methodParameter.getName()));
                methodParameter.setType(type);
                return new MethodParameter[]{methodParameter};
            }
        } else {
            //实例化当前参数对象
            Object obj = ReflectUtil.newInstance(type);
            Field[] fields = type.getDeclaredFields();
            if (fields != null) {
                List<MethodParameter> methodParameters = new ArrayList<>();
                for (Field field : fields) {
                    MethodParameter methodParameter = new MethodParameter(prefix+field.getName());
                    methodParameter.setObj(obj);
                    methodParameter.setType(field.getType());
                    methodParameter.setField(field);
                    if (hasBasciType(field.getType())) {
                        methodParameter.setValue(requestParameters.get(methodParameter.getName()));
                    }else if(hasPrefix(requestParameters,methodParameter.getName()+".")){
                        // createBy.userName
                        // createBy.id
                        MethodParameter[] parameters = getParameterName(methodParameter.getName() + ".", field.getType(), field.getType(), requestParameters);
                        //将属性的实例与当前字段关联
                        if(parameters != null && parameters.length>0){
                            if(parameters[0].getObj() != null){
                                methodParameter.setValue(parameters[0].getObj());
                            }
                        }
                        methodParameters.addAll(Arrays.asList(parameters));
                    }
                    methodParameters.add(methodParameter);
                }
                return methodParameters.toArray(new MethodParameter[0]);
            }
        }
        return new MethodParameter[0];
    }

    private Map<String, String[]> getRequestParameters(HttpServletRequest request) {
        Map<String, String[]> requestParameters = new HashMap<>();
        Enumeration<String> parameterNames = request.getParameterNames();
        while (parameterNames.hasMoreElements()) {
            String name = parameterNames.nextElement();
            requestParameters.put(name, request.getParameterValues(name));
        }
        return requestParameters;
    }
}
