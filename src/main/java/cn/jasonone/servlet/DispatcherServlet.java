package cn.jasonone.servlet;

import cn.hutool.core.util.StrUtil;
import cn.jasonone.annotation.Controller;
import cn.jasonone.annotation.RequestMapping;
import cn.jasonone.annotation.ResponseBody;
import cn.jasonone.handler.HandlerMapping;
import cn.jasonone.handler.HandlerParameters;
import cn.jasonone.model.ControllerDefinition;
import cn.jasonone.model.ModelAndView;
import cn.jasonone.resolver.ViewResolver;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

@Slf4j
public class DispatcherServlet extends HttpServlet {
    private ApplicationContext applicationContext;

    private HandlerMapping handlerMapping;

    private HandlerParameters handlerParameters;

    private ViewResolver viewResolver;

    private Gson gson=new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").serializeNulls().create();

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 1. 找到所有的处理器
        // 2. 找出处理器中所有的url

        String url = req.getRequestURI().substring(req.getContextPath().length());
        // 从映射关系处理器中获得Controller的定义
        ControllerDefinition controllerDefinition = handlerMapping.getController(url);
        if (controllerDefinition == null) {
            resp.sendError(404,"找不到处理器:"+url);
            return;
        }
        // 获取url所映射的方法
        Method method = controllerDefinition.getMethod(url);
        // 获取url所映射的处理器实例
        Object controller = controllerDefinition.getController();
        try {
            //检测类上是否拥有ResponseBody注解
            boolean json=false;
            if(controller.getClass().isAnnotationPresent(ResponseBody.class)){
                json=true;
            }
            if(method.isAnnotationPresent(ResponseBody.class)){
                json=true;
            }
            ModelAndView modelAndView=new ModelAndView();
            Object[] parameters = handlerParameters.getParameters(modelAndView,method, req, resp);
            Object view = method.invoke(controller, parameters);
            // ModelAndView
            // String
            // void

            if(view == null){
                if(StrUtil.isNotBlank(modelAndView.getViewName()) && json==false){
                    // 视图解析
                    viewResolver.handler(modelAndView,req,resp);
                }else{
                    resp.getWriter().print("null");
                }
            }else if(view instanceof ModelAndView){
                ModelAndView mav= (ModelAndView) view;
                modelAndView.setViewName(mav.getViewName());
                modelAndView.putAll(mav);
                viewResolver.handler(modelAndView,req,resp);
            }else if(view instanceof String){
                modelAndView.setViewName(Objects.toString(view));
                if(json == false){
                    viewResolver.handler(modelAndView,req,resp);
                }else{
                    resp.getWriter().print(view);
                }
            }else{
                resp.getWriter().print(gson.toJson(view));
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        applicationContext= WebApplicationContextUtils.getWebApplicationContext(config.getServletContext());
        //从容器中获得映射关系处理器
        handlerMapping=applicationContext.getBean("handlerMapping",HandlerMapping.class);
        handlerParameters=applicationContext.getBean("handlerParameters",HandlerParameters.class);
        viewResolver=applicationContext.getBean("viewResolver",ViewResolver.class);
    }
}
